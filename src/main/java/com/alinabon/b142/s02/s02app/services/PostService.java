package com.alinabon.b142.s02.s02app.services;

import com.alinabon.b142.s02.s02app.models.Post;

public interface PostService {
    void createPost(Post newPost);
    void updatePost(Long id, Post existingPost);
    void deletePost(Long id);
    Iterable<Post> getPosts();
}
